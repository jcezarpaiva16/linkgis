**LinkGIS**
---------------------------------------------------------------------------------------
Software de manipulação e visualização de dados geográficos desenvolvido pelo laboratório LinkDataPop do IESC na Unifersidade Federal do Rio de Janeiro.  

![Screenshot da tela inicial do LinkGIS em 12/07/2020](https://gitlab.com/jcezarpaiva16/linkgis/-/raw/20200711-add-geolocarion/ss1.png)  

> Este software usa bibliotecas e ferramentas open source para
> implementar um criador de mapas dinâmicos em formato HTML.

-------------------------------------------------------------------------------------
 **1. O fluxo**
**- 1.1.** Primeiro é implementada a geolocalização dos dados utilizando-se a API [Nominatim](https://nominatim.openstreetmap.org/) através da biblioteca [Geopy](https://geopy.readthedocs.io/en/stable/) no Python 3;  

> As bases de dados usadas no georreferenciamento são cortesia do [OSM](https://www.openstreetmap.org/), obtidas através do [Geofabrik](https://www.geofabrik.de/)  

**- 1.2.** Depois, os dados já geolocalizados são preparados para serem plotados;  
**- 1.3.** Em seguida, os dados são plotados utilizando-se a biblioteca [Leaflet](https://leafletjs.com/) implementada em Python pela biblioteca [Folium](https://python-visualization.github.io/folium/);  
**- 1.4.** O mapa .html criado pela implementação do Leaflet pelo Folium é carregado em uma WebView do [Qt](https://www.qt.io/) utilizando-se [PyQt5](https://www.riverbankcomputing.com/software/pyqt/);  
**- 1.5.** O PyQt5 fica encarregado de exibir as informações e conectar a o usuário às ferramentas de plotagem aplicando uma interface visual fácil de ser utilizada.  

**2. Licenças**
**- 2.1.** O código fonte do Nominatim é disponibilizado sob a GPLv2 license. Os dados são fornecidos e licenciados pela sob os termos da [OpenStreetMap Foundation](https://osmfoundation.org/) (OSMF);  
**- 2.2.** A biblioteca Geopy é licenciada pela [MIT License](https://github.com/geopy/geopy/blob/master/LICENSE);  
**- 2.3.** O OpenStreetMap tem dados abertos e é licenciado pela [Open Data Commons Open Database License](https://opendatacommons.org/licenses/odbl/) (ODbL) pela [OpenStreetMap Foundation](https://osmfoundation.org/) (OSMF);  
**- 2.4.** [TODO] O Geofabrik...  
**- 2.5.** A biblioteca [Leaflet é licenciado sob a BSD 2-Clause "Simplified" License](https://github.com/Leaflet/Leaflet/blob/master/LICENSE);  
**- 2.6.** A biblioteca [Folium é licenciada sob a MIT License](https://github.com/python-visualization/folium/blob/master/LICENSE.txt);  
**- 2.7.** O PyQt possui licença dupla em todas as plataformas suportadas sob a [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.pt-br.html) e a Riverbank Commercial License.
