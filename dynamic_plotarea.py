from copy import deepcopy

from PyQt5.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                          QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PyQt5.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                         QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
                         QPixmap, QRadialGradient)

from PyQt5.QtWidgets import *
from ui_mainwindow import Ui_MainWindow
from leaflet import Map
from collections import namedtuple

# for indice in range (0, 10):
#     DynamicPlotStruct = namedtuple("DynamicPlotStruct", "var"+indice.__str__+" var"+indice.__str__+" var"+indice.__str__+" var"+indice.__str__+" var"+indice.__str__+" ")


class Dynamic_PlotArea(dict):
    def __init__(self, value):
        self.valid_keys = list(value)
        super().__init__(value)

    def __setitem__(self, name, value):
        if name not in self.valid_keys:
            raise KeyError(f"Invalid key: {name!r}")
        super().__setitem__(name, value)

    def copy_set(self, new, key, value):
        super(type(new), new).__setitem__(key, value)

    def update(self, items):
        if isinstance(items, dict):
            items = items.items()
        for key, value in items:
            self.__setitem__(key, value)

    def copy_update(self, items):
        if isinstance(items, dict):
            items = items.items()
        for key, value in items:
            self.copy_set(self, key, value)

    def __copy__(self):
        cls = self.__class__
        result = cls.__new__(cls)
        result.__dict__.copy_update(self.__dict__)
        return result

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.items():
            self.copy_set(result, k, deepcopy(v))
        return result

    def __repr__(self):
        return '{%s}' % ', '.join([': '.join([repr(k), str(v)]) \
                                   for k, v in zip(self.keys(), self.values())])
