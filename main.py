# This Python file uses the following encoding: utf-8
import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from PyQt5.QtCore import QUrl, QDir, QSize
from PyQt5.QtWebEngineWidgets import QWebEngineView

from ui_mainwindow import Ui_MainWindow
from leaflet import Map

# defining home and current path
home_path = QDir.homePath()
current_path = QDir.currentPath()


class MainWindow(QMainWindow, Ui_MainWindow):
    output_map = current_path + '/.my_map.html'
    file_name = ''

    def __init__(self, *args, obj=None, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)

#        var = self.popup_type
#        self.data_type
#        self.lat_index
#        self.lon_index
#        self.plot_size
        

        self.mapFrame = QWebEngineView(self.centralwidget)
        self.mapFrame.load(QUrl.fromLocalFile(self.output_map))
        self.mapFrame.setObjectName(u"mapFrame")
        self.mapFrame.setMinimumSize(QSize(707, 555))
        self.gridLayout.addWidget(self.mapFrame, 0, 1, 1, 2)

    def reloadMap(self):
        map_instance = Map()
        map_instance.createMap([-22.8, -43.5],
                               self.styleComboBox.currentText(),
                               10, self.output_map)
        self.mapFrame.reload()

    def openFile(self):
        self.file_name, _ = QFileDialog.getOpenFileName(self, "Abrir arquivo de dados", QDir.homePath(),
                                                        "Arquivos .shp, .geojson, .json, .csv"
                                                        "(*.shp *.geojson *.json *.csv)")
        if self.file_name:
            self.latLabel.setText(self.file_name)

    def saveMap(self):
        # TODO: função getMapOptions para salvar e recarregar os mapas
        save_file_name, _ = QFileDialog.getSaveFileName(self, "Salvar dados .csv", QDir.homePath(),
                                                     "Arquivo .html (*.html)")
        if save_file_name:
            map_instance = Map()
            map_instance.createMap([-22.8, -43.5],
                                   self.styleComboBox.currentText(),
                                   10, save_file_name)

#    def getColor(self):


    def getPlotInfo(self):
        popup_type = self.popupComboBox.currentText()
        data_type = self.dataFormatComboBox.currentText()
        lat_index = self.latComboBox.currentIndex()
        lon_index = self.latComboBox.currentIndex()
        plot_size = self.sizeSpinBox.value()

        if popup_type and data_type and lat_index and lon_index and plot_size != 0:
            self.popup_type = popup_type
            self.data_type = data_type
            self.lat_index = lat_index
            self.lon_index = lon_index
            self.plot_size = plot_size



    def makeConnections(self):
        # reload map when reloadButton is pressed
        self.reloadButton.clicked.connect(self.reloadMap)

        # make connections from map to progressbar
        self.mapFrame.loadProgress.connect(lambda p:
                                           self.progressBar.setValue(p))

        # make connection from file menu to QFileDialog
        self.actionAbrir.triggered.connect(lambda _open: self.openFile())
        self.actionSalvar_mapa_como_2.triggered.connect(lambda _save: self.saveMap())

        # make connection from exit on menu to exit():
        self.actionSair.triggered.connect(app.exit)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()

    # Load style_list to the map_style_combobox
    style_list = ["Stamen Terrain", "Stamen Toner", "Mapbox Bright",
                  "cartodbdark_matter", "cartodbpositron", "openstreetmap"]
    window.styleComboBox.addItems(style_list)

    web_map = Map()
    web_map.createMap([-22.8, -43.5], window.styleComboBox.currentText(),
                      10, window.output_map)

    window.makeConnections()

    window.show()

    sys.exit(app.exec_())
