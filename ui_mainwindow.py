# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PyQt5.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PyQt5.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PyQt5.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1010, 666)
        self.actionAbrir_arquivo_de_dados = QAction(MainWindow)
        self.actionAbrir_arquivo_de_dados.setObjectName(u"actionAbrir_arquivo_de_dados")
        self.actionSalvar_mapa_como = QAction(MainWindow)
        self.actionSalvar_mapa_como.setObjectName(u"actionSalvar_mapa_como")
        self.actionSair = QAction(MainWindow)
        self.actionSair.setObjectName(u"actionSair")
        self.actionSalvar = QAction(MainWindow)
        self.actionSalvar.setObjectName(u"actionSalvar")
        self.actionAbrir = QAction(MainWindow)
        self.actionAbrir.setObjectName(u"actionAbrir")
        self.actionAbrir_recentes = QAction(MainWindow)
        self.actionAbrir_recentes.setObjectName(u"actionAbrir_recentes")
        self.actionSalvar_dados_como = QAction(MainWindow)
        self.actionSalvar_dados_como.setObjectName(u"actionSalvar_dados_como")
        self.actionSalvar_mapa_como_2 = QAction(MainWindow)
        self.actionSalvar_mapa_como_2.setObjectName(u"actionSalvar_mapa_como_2")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.reloadButton = QPushButton(self.centralwidget)
        self.reloadButton.setObjectName(u"reloadButton")

        self.gridLayout.addWidget(self.reloadButton, 2, 0, 1, 1)

        self.scrollArea = QScrollArea(self.centralwidget)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setMinimumSize(QSize(261, 0))
        self.scrollArea.setMaximumSize(QSize(261, 16777215))
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 259, 571))
        self.line = QFrame(self.scrollAreaWidgetContents)
        self.line.setObjectName(u"line")
        self.line.setGeometry(QRect(12, 43, 239, 3))
        self.line.setMaximumSize(QSize(239, 3))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)
        self.addPlotButton = QPushButton(self.scrollAreaWidgetContents)
        self.addPlotButton.setObjectName(u"addPlotButton")
        self.addPlotButton.setGeometry(QRect(12, 52, 88, 25))
        self.addPlotButton.setMaximumSize(QSize(101, 25))
        self.layoutWidget = QWidget(self.scrollAreaWidgetContents)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 241, 27))
        self.styleHorizontalLayout = QHBoxLayout(self.layoutWidget)
        self.styleHorizontalLayout.setObjectName(u"styleHorizontalLayout")
        self.styleHorizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.styleLabel = QLabel(self.layoutWidget)
        self.styleLabel.setObjectName(u"styleLabel")
        self.styleLabel.setMaximumSize(QSize(84, 16777215))

        self.styleHorizontalLayout.addWidget(self.styleLabel)

        self.styleComboBox = QComboBox(self.layoutWidget)
        self.styleComboBox.setObjectName(u"styleComboBox")

        self.styleHorizontalLayout.addWidget(self.styleComboBox)

        self.layoutWidget1 = QWidget(self.scrollAreaWidgetContents)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(10, 80, 241, 258))
        self.plotVerticalLayout = QVBoxLayout(self.layoutWidget1)
        self.plotVerticalLayout.setObjectName(u"plotVerticalLayout")
        self.plotVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self.plotNumberHorizontalLayout = QHBoxLayout()
        self.plotNumberHorizontalLayout.setObjectName(u"plotNumberHorizontalLayout")
        self.plotNumberLabel = QLabel(self.layoutWidget1)
        self.plotNumberLabel.setObjectName(u"plotNumberLabel")
        self.plotNumberLabel.setMaximumSize(QSize(51, 17))

        self.plotNumberHorizontalLayout.addWidget(self.plotNumberLabel)

        self.plotNumberLine = QFrame(self.layoutWidget1)
        self.plotNumberLine.setObjectName(u"plotNumberLine")
        self.plotNumberLine.setFrameShape(QFrame.HLine)
        self.plotNumberLine.setFrameShadow(QFrame.Sunken)

        self.plotNumberHorizontalLayout.addWidget(self.plotNumberLine)


        self.plotVerticalLayout.addLayout(self.plotNumberHorizontalLayout)

        self.popupHorizontalLayout = QHBoxLayout()
        self.popupHorizontalLayout.setObjectName(u"popupHorizontalLayout")
        self.popupLabel = QLabel(self.layoutWidget1)
        self.popupLabel.setObjectName(u"popupLabel")
        self.popupLabel.setMaximumSize(QSize(51, 16777215))

        self.popupHorizontalLayout.addWidget(self.popupLabel)

        self.popupComboBox = QComboBox(self.layoutWidget1)
        self.popupComboBox.addItem("")
        self.popupComboBox.addItem("")
        self.popupComboBox.addItem("")
        self.popupComboBox.addItem("")
        self.popupComboBox.setObjectName(u"popupComboBox")

        self.popupHorizontalLayout.addWidget(self.popupComboBox)


        self.plotVerticalLayout.addLayout(self.popupHorizontalLayout)

        self.dataHorizontalLayout = QHBoxLayout()
        self.dataHorizontalLayout.setObjectName(u"dataHorizontalLayout")
        self.dataFormatLabel = QLabel(self.layoutWidget1)
        self.dataFormatLabel.setObjectName(u"dataFormatLabel")
        self.dataFormatLabel.setEnabled(True)

        self.dataHorizontalLayout.addWidget(self.dataFormatLabel)

        self.dataFormatComboBox = QComboBox(self.layoutWidget1)
        self.dataFormatComboBox.addItem("")
        self.dataFormatComboBox.addItem("")
        self.dataFormatComboBox.setObjectName(u"dataFormatComboBox")
        self.dataFormatComboBox.setEnabled(True)

        self.dataHorizontalLayout.addWidget(self.dataFormatComboBox)


        self.plotVerticalLayout.addLayout(self.dataHorizontalLayout)

        self.latlonGridLayout = QGridLayout()
        self.latlonGridLayout.setObjectName(u"latlonGridLayout")
        self.latLabel = QLabel(self.layoutWidget1)
        self.latLabel.setObjectName(u"latLabel")
        self.latLabel.setEnabled(False)
        self.latLabel.setMaximumSize(QSize(53, 16777215))

        self.latlonGridLayout.addWidget(self.latLabel, 0, 0, 1, 1)

        self.latComboBox = QComboBox(self.layoutWidget1)
        self.latComboBox.setObjectName(u"latComboBox")
        self.latComboBox.setEnabled(False)

        self.latlonGridLayout.addWidget(self.latComboBox, 0, 1, 1, 2)

        self.lonLabel = QLabel(self.layoutWidget1)
        self.lonLabel.setObjectName(u"lonLabel")
        self.lonLabel.setEnabled(False)
        self.lonLabel.setMaximumSize(QSize(16777215, 25))

        self.latlonGridLayout.addWidget(self.lonLabel, 1, 0, 1, 1)

        self.lonComboBox = QComboBox(self.layoutWidget1)
        self.lonComboBox.setObjectName(u"lonComboBox")
        self.lonComboBox.setEnabled(False)

        self.latlonGridLayout.addWidget(self.lonComboBox, 1, 1, 1, 2)


        self.plotVerticalLayout.addLayout(self.latlonGridLayout)

        self.colorHorizontalLayout = QHBoxLayout()
        self.colorHorizontalLayout.setObjectName(u"colorHorizontalLayout")
        self.colorLabel = QLabel(self.layoutWidget1)
        self.colorLabel.setObjectName(u"colorLabel")
        self.colorLabel.setEnabled(False)
        self.colorLabel.setMaximumSize(QSize(54, 16777215))

        self.colorHorizontalLayout.addWidget(self.colorLabel)

        self.colorButton = QPushButton(self.layoutWidget1)
        self.colorButton.setObjectName(u"colorButton")
        self.colorButton.setEnabled(False)

        self.colorHorizontalLayout.addWidget(self.colorButton)


        self.plotVerticalLayout.addLayout(self.colorHorizontalLayout)

        self.sizeHorizontalLayout = QHBoxLayout()
        self.sizeHorizontalLayout.setObjectName(u"sizeHorizontalLayout")
        self.sizeLabel = QLabel(self.layoutWidget1)
        self.sizeLabel.setObjectName(u"sizeLabel")
        self.sizeLabel.setEnabled(False)

        self.sizeHorizontalLayout.addWidget(self.sizeLabel)

        self.sizeSpinBox = QSpinBox(self.layoutWidget1)
        self.sizeSpinBox.setObjectName(u"sizeSpinBox")
        self.sizeSpinBox.setEnabled(False)
        self.sizeSpinBox.setMaximumSize(QSize(61, 16777215))

        self.sizeHorizontalLayout.addWidget(self.sizeSpinBox)


        self.plotVerticalLayout.addLayout(self.sizeHorizontalLayout)

        self.removePlotButton = QPushButton(self.layoutWidget1)
        self.removePlotButton.setObjectName(u"removePlotButton")

        self.plotVerticalLayout.addWidget(self.removePlotButton)

        self.line_2 = QFrame(self.layoutWidget1)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.plotVerticalLayout.addWidget(self.line_2)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout.addWidget(self.scrollArea, 0, 0, 1, 1)

        self.progressBar = QProgressBar(self.centralwidget)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setValue(24)

        self.gridLayout.addWidget(self.progressBar, 2, 1, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1010, 22))
        self.menuArquivos = QMenu(self.menubar)
        self.menuArquivos.setObjectName(u"menuArquivos")
        self.menuArquivos_2 = QMenu(self.menuArquivos)
        self.menuArquivos_2.setObjectName(u"menuArquivos_2")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuArquivos.menuAction())
        self.menuArquivos.addAction(self.menuArquivos_2.menuAction())
        self.menuArquivos.addAction(self.actionSalvar)
        self.menuArquivos.addSeparator()
        self.menuArquivos.addAction(self.actionSair)
        self.menuArquivos_2.addAction(self.actionAbrir)
        self.menuArquivos_2.addAction(self.actionAbrir_recentes)
        self.menuArquivos_2.addSeparator()
        self.menuArquivos_2.addAction(self.actionSalvar_dados_como)
        self.menuArquivos_2.addAction(self.actionSalvar_mapa_como_2)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.actionAbrir_arquivo_de_dados.setText(QCoreApplication.translate("MainWindow", u"Abrir arquivo de dados", None))
        self.actionSalvar_mapa_como.setText(QCoreApplication.translate("MainWindow", u"Salvar mapa como...", None))
        self.actionSair.setText(QCoreApplication.translate("MainWindow", u"Sair", None))
        self.actionSalvar.setText(QCoreApplication.translate("MainWindow", u"Configurar dados", None))
        self.actionAbrir.setText(QCoreApplication.translate("MainWindow", u"Abrir...", None))
        self.actionAbrir_recentes.setText(QCoreApplication.translate("MainWindow", u"Abrir recentes...", None))
        self.actionSalvar_dados_como.setText(QCoreApplication.translate("MainWindow", u"Salvar dados como...", None))
        self.actionSalvar_mapa_como_2.setText(QCoreApplication.translate("MainWindow", u"Salvar mapa como...", None))
        self.reloadButton.setText(QCoreApplication.translate("MainWindow", u"Recarregar", None))
        self.addPlotButton.setText(QCoreApplication.translate("MainWindow", u"Adicionar plot", None))
        self.styleLabel.setText(QCoreApplication.translate("MainWindow", u"Estilo do mapa", None))
        self.plotNumberLabel.setText(QCoreApplication.translate("MainWindow", u"Plot [i]", None))
        self.popupLabel.setText(QCoreApplication.translate("MainWindow", u"Popup", None))
        self.popupComboBox.setItemText(0, QCoreApplication.translate("MainWindow", u"Pontos", None))
        self.popupComboBox.setItemText(1, QCoreApplication.translate("MainWindow", u"Cluster de pontos", None))
        self.popupComboBox.setItemText(2, QCoreApplication.translate("MainWindow", u"Mapa de calor", None))
        self.popupComboBox.setItemText(3, QCoreApplication.translate("MainWindow", u"Mapa de cloropleta", None))

        self.dataFormatLabel.setText(QCoreApplication.translate("MainWindow", u"Formato dos dados", None))
        self.dataFormatComboBox.setItemText(0, QCoreApplication.translate("MainWindow", u"Geometry", None))
        self.dataFormatComboBox.setItemText(1, QCoreApplication.translate("MainWindow", u"Lat/Lon", None))

        self.latLabel.setText(QCoreApplication.translate("MainWindow", u"Lat", None))
        self.lonLabel.setText(QCoreApplication.translate("MainWindow", u"Lon", None))
        self.colorLabel.setText(QCoreApplication.translate("MainWindow", u"Cor", None))
        self.colorButton.setText(QCoreApplication.translate("MainWindow", u"Selecionar...", None))
        self.sizeLabel.setText(QCoreApplication.translate("MainWindow", u"Tamanho", None))
        self.removePlotButton.setText(QCoreApplication.translate("MainWindow", u"Remover plot", None))
        self.menuArquivos.setTitle(QCoreApplication.translate("MainWindow", u"Arquivos", None))
        self.menuArquivos_2.setTitle(QCoreApplication.translate("MainWindow", u"Arquivos", None))
    # retranslateUi

