import folium
from folium.plugins import MarkerCluster
from folium.plugins import HeatMap

import geopandas as gpd

class Map(folium.Map):
    def createMap(self, loc, tile, zoom, output_file):
        my_map = folium.Map(location=loc, tiles=tile, zoom_start=zoom, control_scale=True)
        my_map.save(output_file)
        return my_map

    def plotPoint(self, my_map, df, ad_index, lat_index, lon_index, _fill_color, _radius):
        for idx, row in df.iterrows():
            # Get lat and lon of points
            lon = row[lon_index]
            lat = row[lat_index]

            # Get address information
            address = row[ad_index]
            # Add marker to the map
            folium.Circle(location=[lat, lon],
                          popup=address,
                          fill_color=_fill_color,
                          radius=_radius).add_to(my_map)
            # folium.Marker(location=[lat, lon],
            #               popup=address,
            #               fill_color='#2b8cbe',
            #               number_of_sides=6,
            #               radius=8).add_to(my_map)

    def plotCluster(self, my_map, df, ad_index, lat_index, lon_index, _fill_color, _radius):
        # Create a Clustered map where points are clustered
        marker_cluster = MarkerCluster().add_to(my_map)

        # Create Address points on top of the map
        for idx, row in df.iterrows():
            # Get lat and lon of points
            lon = row[lon_index]
            lat = row[lat_index]

            # Get address information
            address = row[ad_index]
            # Add marker to the map
            folium.Marker(location=[lat, lon],
                          popup=address,
                          fill_color=_fill_color,
                          number_of_sides=6,
                          radius=_radius).add_to(marker_cluster)

    def plotHeatMap(self, my_map, df, lat_index, lon_index, _radius):
        heat_data = [[row[lat_index], row[lon_index]] for index, row in df.iterrows()]
        HeatMap(heat_data, name="Mapa de calor", radius=_radius).add_to(my_map)

    def importGeojson(self, my_map, geojson_file):
        # Importing polygon limits to map
        folium.GeoJson(
            geojson_file,
            name='geojson from: ' + geojson_file
        ).add_to(my_map)
